FROM arm32v7/ruby:2.6-alpine

RUN apk add --no-cache \
            git \
            make \
            g++ \
            bash \
&& gem install github-markup \
&& gem install github-markdown \
&& cd /usr/local/bin \
&& git clone https://gitlab.com/edge226/ghmd-ccs.git \
&& apk del --no-cache \
            g++ \
            make \
            git 

ENV PATH /bin:/usr/bin:/usr/local/bin/ghmd-ccs:$PATH

CMD ["bash"]