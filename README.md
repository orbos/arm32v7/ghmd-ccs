# ghmd-ccs

This will create a functional raspberry pi 3 docker image to use [ghmd-ccs](https://gitlab.com/edge226/ghmd-ccs)

I use this tool to create my static websites from markdown and I figured having a better docker image to do this would be nice.

Daily builds of the [docker image](https://gitlab.com/docker-rpi3/ghmd-ccs/container_registry) happen at 6am Mountain timezone and are done on a [Raspberry Pi 3 Model B](https://www.raspberrypi.org/products/raspberry-pi-3-model-b/).
